// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/testslim/', 
  firebase:{
    apiKey: "AIzaSyCChcVz9jUXKl0oZ_zC86rZRykI_CL2baY",
    authDomain: "exampletest-e90f6.firebaseapp.com",
    databaseURL: "https://exampletest-e90f6.firebaseio.com",
    projectId: "exampletest-e90f6",
    storageBucket: "exampletest-e90f6.appspot.com",
    messagingSenderId: "857390714277"
  }
};
