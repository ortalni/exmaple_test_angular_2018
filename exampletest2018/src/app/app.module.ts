import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from './users/users.service';
import { HttpModule } from '@angular/http';
import{ FormsModule,ReactiveFormsModule} from "@angular/forms";
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UsersfComponent } from './usersf/usersf.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersFormComponent,
    UsersfComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,    
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'navigation', component:NavigationComponent}, // localhost:4200
      {path:'', component:UsersComponent}, //default - localhost:4200 - homepage
      {path:'products', component:ProductsComponent}, //localhost:4200/products
      {path:'userForm/:id', component:UsersFormComponent},      
      {path:'usersf', component:UsersfComponent},            
      {path:'**', component:NotFoundComponent} //all the routs that don't exist
      
      
    ])
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }