import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users;
  usersKeys;
  service : UsersService;
  listUserSelection = { name: "", phonenumber : "", key : null};

  constructor(service:UsersService) { 
    this.service = service;
    service.getUsers().subscribe(response => {
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });

  }


  optimisticAdd(user){
    var newKey = this.usersKeys[this.usersKeys.length-1] + 1;    
    this.users[newKey] = user;
    this.usersKeys = Object.keys(this.users);
  }

  pessimisticAdd(){
    this.service.getUsers().subscribe(response => {
    this.users = response.json();
    this.usersKeys = Object.keys(this.users);
  })}


  deleteUser(key){
    console.log(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index, 1);
    //delete from db
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }

  editUser(key) {
    this.listUserSelection.name = this.users[key].name;
    this.listUserSelection.phonenumber = this.users[key].phonenumber;    
    
    this.listUserSelection.key = key;
  }

  ngOnInit() {
  }

}
