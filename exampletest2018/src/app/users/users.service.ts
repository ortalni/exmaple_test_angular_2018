import { environment } from './../../environments/environment';
import {HttpParams} from '@angular/common/http';
import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {AngularFireDatabase} from 'angularfire2/database';


@Injectable()
export class UsersService {
  http:Http;
  db:AngularFireDatabase;


  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
   return this.http.get(environment.url + 'users');
  }
  
  getUsersFire(){
    return this.db.list('/users').valueChanges();
  
  }

  getUser(id){
    return this.http.get(environment.url + 'users/'+ id); 
 
   }
  postUser(data){
    //get users from the SLIM rest API (Don't say DB)
    let options = {
      headers:new Headers({
        'content-type':'application/json;charset=utf-8'
      })
    }
   return this.http.post(environment.url + 'users', data, options);

  }

  deleteUser(key){
    return this.http.delete(environment.url + 'users/'+key);
  }

  putUser(data,key){
    let options = {
      headers:new Headers({
        'content-type':'application/json;charset=utf-8'
      })
    }
    
    
    return this.http.put(environment.url + 'users/' +key, data, options);
    
  }

  constructor(http:Http, db:AngularFireDatabase) {
    this.http = http;
    this.db = db;
   }
 }
