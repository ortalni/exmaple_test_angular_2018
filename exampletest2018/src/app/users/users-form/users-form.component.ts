import { UsersService } from './../users.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import {ActivatedRoute, Router} from'@angular/router';



@Component({
  selector: 'usersForm',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})


export class UsersFormComponent implements OnInit {    
    
    usform = new FormGroup({
      name:new FormControl(),
      phonenumber:new FormControl(),
    });
  constructor( private service:UsersService, private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.usform = this.formBuilder.group({
      name:  [null, [Validators.required]],
      phonenumber: [null, Validators.required],
    });	
}

  @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic
  @Input() selectedUser : { name: String, phonenumber: String, key : number};

  submitButtonText = () => { return this.selectedUser.key ? "Save" : "Add user"};
  
  sendData(){
    console.log("send data");   
    if (this.selectedUser.key) {
      //UPDATE
      this.service.putUser(this.usform.value, this.selectedUser.key).subscribe(
        response => {
          console.log(response.json());          
          this.addUserPs.emit();
          this.usform.reset();
          this.selectedUser.key = null;
          
        },
      error => {
        console.log(error.json());
        this.usform.reset();        
        this.selectedUser.key = null;        
      });
    } else {
      //CREATE
      this.addUser.emit(this.usform.value);              
      this.service.postUser(this.usform.value).subscribe(
        response => {
          console.log(response.json());
          this.addUserPs.emit();
          this.usform.reset();
        },
        error => {
          console.log(error.json());    
        } )
    }

   
  }  

}
